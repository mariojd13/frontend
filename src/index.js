import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes/Routes';

import "bootstrap/dist/css/bootstrap.min.css";
import "./css/general.css";

ReactDOM.render(<Routes />, document.getElementById('root'));

